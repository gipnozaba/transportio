import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from "vue-router";
import Main from './components/MainView/Main'
import User from './components/User/User'
import Driver from './components/Driver/Driver'

Vue.use(VueRouter);

Vue.config.productionTip = false;

const router = new VueRouter({
  routes: [
    {path: '/',component: Main},
    {path: '/users/1',component: User},
    {path: '/driver/0',component: Driver}
  ],
  mode: 'history'
});

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app');
