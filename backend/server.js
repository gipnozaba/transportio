const express = require('express');
const app = express();
const expressLayouts = require('express-ejs-layouts');
const fs = require('fs');


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded());

// Parse JSON bodies (as sent by API clients)
app.use(express.json());


app.get("/client/:id", (req, res, next) =>{
  let id = req.params.id;
  let raw = fs.readFileSync('data.json');
  let data = JSON.parse(raw);
  let clients = data['clients'];


  let client = clients[id]
  //Client info
  let name = client['name'];
  let email = client['email'];
  let phone = client['phone'];

  res.send("Requested client: " + name + ", " + email + " [" + phone + "]");
})

app.get("/driver/:id", (req, res, next) =>{
  let id = req.params.id;
  let raw = fs.readFileSync('data.json');
  let data = JSON.parse(raw);
  let driver = data['drivers'][req.params.id];
  res.send(driver);
})

// Returns all orders
app.get("/orders/", (req, res, next) =>{
  let raw = fs.readFileSync('data.json');
  let data = JSON.parse(raw);
  let orders = data['orders'];
  res.send(orders);
})

// Adds new order
app.post("/orders/", (req, res, next) =>{
  let raw = fs.readFileSync('data.json');
  let data = JSON.parse(raw);
  var new_order = req.body;
  let order_amount = data['orders'].length;

  var order_deadline = new Date(new_order['deadline']);
  var order_short_deadline = order_deadline.getDate() <= 9 ? "0"+order_deadline.getDate() + "." : order_deadline.getDate() + ".";
  order_short_deadline += order_deadline.getMonth() <= 9 ? "0"+order_deadline.getMonth() + "." : order_deadline.getMonth() + ".";
  order_short_deadline += order_deadline.getFullYear();

  new_order['creator'] = "1";
  new_order['status'] = "pending";
  new_order['distance'] = "120";
  new_order['image'] = "link";
  new_order['deadline'] = order_short_deadline;
  new_order['bids'] = [
    {
      "driver":"-1",
      "bid":"∞",
    }
  ];
  new_order['id'] = order_amount.toString();
  data['orders'].push(new_order);


  console.log(new_order);

  fs.writeFile('data.json', JSON.stringify(data), 'utf8', function (err) {
    if (err) {
      return console.log(err);
    }
    console.log("Order saved!");
  });
})

// Adds new bid
app.post("/orders/:id", (req, res, next) =>{
  let raw = fs.readFileSync('data.json');
  let data = JSON.parse(raw);
  let order_id = req.params.id;

  var new_bid = {
    "driver":req.body.driver,
    "bid":req.body.bid
  }

  for (order_num in data['orders']) {
    if(order_num == order_id)
    {
      data['orders'][order_num]['bids'].push(new_bid);

      fs.writeFileSync('data.json', JSON.stringify(data), 'utf8', function (err) {
        if (err) {
          return console.log(err);
        }
        console.log("Bid has been saved!");
      });
      break;
    }
  }
})


// Gets client's orders
app.get("/clients/:idc/orders", (req, res, next) =>{
  let raw = fs.readFileSync('data.json');
  let data = JSON.parse(raw);
  let client = data['clients'][req.params.idc];
  let client_orders = [];

  // Getting all client's orders
  for (var order_num in data['orders']) {
    var order = data['orders'][order_num];
    let order_creator = order['creator'];

    if(order_creator == req.params.idc){
      client_orders.push(order);
    }
  }

  res.send(client_orders);
})


app.listen(1337, function() {
  console.log('Started!');
})
